<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_category_if_exist()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $category = Category::factory()->create();
        $countCategoryBefore = Category::count();
        $response = $this->delete(route('category.destroy', $category->id));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseCount('categories', $countCategoryBefore - 1);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete(route('category.destroy', $category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

    /** @test */
    public function authenticated_user_can_not_delete_category_if_category_not_exist()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $categoryId = -1;
        $response = $this->delete(route('category.destroy', $categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
