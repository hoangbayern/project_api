<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_new_category()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $categoryData = Category::factory()->make()->toArray();
        $countCategoryBefore = Category::count();
        $response = $this->post(route('category.store'), $categoryData);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseCount('categories', $countCategoryBefore + 1);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_category()
    {
        $categoryData = Category::factory()->make()->toArray();
        $response = $this->post(route('category.store'), $categoryData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

    /** @test */
    public function authenticated_user_can_not_create_category_if_name_field_is_null()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $categoryData = Category::factory()->make(['name' => null])->toArray();
        $response = $this->post(route('category.store'), $categoryData);
        $response->assertSessionHasErrors('name');
    }
}
