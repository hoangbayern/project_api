<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use function MongoDB\BSON\toJSON;

class UpdateCategoryTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function authenticated_user_can_update_category()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataCategory = Category::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name
        ];
        $response = $this->put(route('category.update', $dataCategory->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $dataAfter = Category::find($dataCategory->id);
        $dataUpAfter = [
            'name' => $dataAfter->name
        ];
        $this->assertEquals($dataUpdate, $dataUpAfter, message: 'no update');
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $dataCategory = Category::factory()->create();
        $response = $this->put(route('category.update', $dataCategory->id), $dataCategory->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_name_is_null()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataCategory = Category::factory()->create();
        $dataUp = [
            'name' => ''
        ];
        $response = $this->put(route('category.update', $dataCategory->id), $dataUp);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_category_is_not_exist()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataCategoryId = -1;
        $dataUpdate = Category::factory()->make()->toArray();
        $response = $this->put(route('category.update', $dataCategoryId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
