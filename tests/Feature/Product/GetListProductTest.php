<?php

namespace Tests\Feature\Product;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_product()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('product.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.product.list');
        $response->assertSee($user->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_product()
    {
        $response = $this->get(route('product.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

}
