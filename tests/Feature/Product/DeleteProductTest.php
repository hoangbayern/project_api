<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_delete_product_if_product_exist(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $productCountBefore = Product::count();
        $response = $this->delete(route('product.destroy',$product->id));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseCount('products',$productCountBefore-1);
    }

    /** @test  */
    public function unauthenticated_user_can_not_delete_product(){
        $product = Product::factory()->create();
        $response = $this->delete(route('product.destroy',$product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

    /** @test  */
    public function authenticated_user_can_not_delete_product_if_product_not_exist(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $productId = -1;
        $response = $this->delete(route('product.destroy',$productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
