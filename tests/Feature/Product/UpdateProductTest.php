<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    use WithFaker;
    /** @test  */
    public function authenticated_user_can_update_product_if_product_exist(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->create();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->put(route('product.update',$dataProduct->id),$dataProduct->toArray());
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function unauthenticated_user_can_not_update_product(){
        Storage::disk('image');
        $dataProduct = Product::factory()->create();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->put(route('product.update',$dataProduct->id),$dataProduct->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('admin_login');
    }

    /** @test  */
    public function authenticated_user_can_not_update_product_if_product_not_exist(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->create();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->put(route('product.update',-1),$dataProduct->toArray());
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function authenticated_user_can_not_update_product_if_name_field_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataProduct = Product::factory()->create();
        $product = [
            'name' => '',
            'image' => $this->faker->imageUrl(150, 150),
            'description' => $this->faker->paragraph(),
            'stock' => $this->faker->randomNumber(2),
            'price' => $this->faker->randomNumber(6),
            'activated' => $this->faker->boolean(),
        ];
        $response = $this->put(route('product.update',$dataProduct->id),$product);
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function authenticated_user_can_not_update_product_if_description_field_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataProduct = Product::factory()->create();
        $product = [
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl(150, 150),
            'description' => '',
            'stock' => $this->faker->randomNumber(2),
            'price' => $this->faker->randomNumber(6),
            'activated' => $this->faker->boolean(),
        ];
        $response = $this->put(route('product.update',$dataProduct->id),$product);
        $response->assertSessionHasErrors('description');
    }

    /** @test  */
    public function authenticated_user_can_not_update_product_if_stock_field_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataProduct = Product::factory()->create();
        $product = [
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl(150, 150),
            'description' => $this->faker->paragraph,
            'stock' => '',
            'price' => $this->faker->randomNumber(6),
            'activated' => $this->faker->boolean(),
        ];
        $response = $this->put(route('product.update',$dataProduct->id),$product);
        $response->assertSessionHasErrors('stock');
    }

    /** @test  */
    public function authenticated_user_can_not_update_product_if_price_field_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $dataProduct = Product::factory()->create();
        $product = [
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl(150, 150),
            'description' => $this->faker->paragraph,
            'stock' => $this->faker->randomNumber(2),
            'price' => '',
            'activated' => $this->faker->boolean(),
        ];
        $response = $this->put(route('product.update',$dataProduct->id),$product);
        $response->assertSessionHasErrors('price');
    }
}
