<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;


class CreateProductTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_create_new_product(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $countProductBefore = Product::count();
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->post(route('product.store'),$dataProduct);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseCount('products',$countProductBefore+1);
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_new_product(){
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->post(route('product.store'),$dataProduct);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

    /** @test  */
    public function authenticated_user_can_not_create_new_product_if_field_name_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->make(['name'=>null])->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->post(route('product.store'),$dataProduct);
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function authenticated_user_can_not_create_new_product_if_field_stock_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->make(['stock'=>null])->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->post(route('product.store'),$dataProduct);
        $response->assertSessionHasErrors('stock');
    }

    /** @test  */
    public function authenticated_user_can_not_create_new_product_if_field_price_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->make(['price'=>null])->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->post(route('product.store'),$dataProduct);
        $response->assertSessionHasErrors('price');
    }

    /** @test  */
    public function authenticated_user_can_not_create_new_product_if_field_description_is_null(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->make(['description'=>null])->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->post(route('product.store'),$dataProduct);
        $response->assertSessionHasErrors('description');
    }
}
