<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowProductTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_show_details_product_if_product_exist(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $response = $this->get(route('product.show',$product->id));
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function unauthenticated_user_can_not_show_details_product(){
        $product = Product::factory()->create();
        $response = $this->get(route('product.show',$product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/admin_login');
    }

    /** @test  */
    public function authenticated_user_can_not_show_details_product_if_product_not_exist(){
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('product.show',-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
