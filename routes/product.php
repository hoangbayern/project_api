<?php

use App\Http\Controllers\Admin\ProductController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin/products')->group(function (){
    Route::controller(ProductController::class)->group(function (){
        Route::as('product.')->group(function (){
            Route::get('/list','index')->name('index')
                ->middleware('auth')
            ->middleware('permission:product.index');
            Route::get('/active-product/{id}','active')->name('active');
            Route::get('/inactive-product/{id}','inactive')->name('inactive');
            Route::delete('/{id}','destroy')->name('destroy')
                ->middleware('auth')
            ->middleware('permission:product.destroy');
            Route::get('/create','create')->name('create')
            ->middleware('permission:product.create');
            Route::post('/create/store','store')->name('store')
                ->middleware('auth')
            ->middleware('permission:product.create');
            Route::get('/update/{id}','edit')->name('edit')
            ->middleware('permission:product.update');
            Route::put('/update/product/{id}','update')->name('update')
                ->middleware('auth')
            ->middleware('permission:product.update');
            Route::get('/show/{id}','show')->name('show')
                ->middleware('auth');
            Route::get('/search-list','productListAjax')->name('productListAjax');
            Route::get('/search-product','search')->name('searchProduct');
        });
    });
});
