<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CategoryController;

Route::prefix('admin/categories')->group(function (){
    Route::controller(CategoryController::class)->group(function (){
        Route::as('category.')->group(function (){
            Route::get('/create','create')->name('create')
                ->middleware('permission:category.create');
            Route::post('/','store')->name('store')
                ->middleware('auth')
            ->middleware('permission:category.create');
            Route::put('/update/{id}','update')->name('update')
                ->middleware('auth')
            ->middleware('permission:category.update');
            Route::get('/edit/{id}','edit')->name('edit')
            ->middleware('permission:category.update');
            Route::get('/list','index')->name('index')
                ->middleware('auth')
            ->middleware('permission:category.index');
            Route::get('/active-category/{id}','active')->name('active');
            Route::get('/inactive-category/{id}','inactive')->name('inactive');
            Route::delete('/{id}','destroy')->name('destroy')
                ->middleware('auth')
                ->middleware('permission:category.destroy');
            Route::get('/search-list','categoryListAjax')->name('categoryListAjax');
            Route::get('/search-category','search')->name('searchCategory');
        });
    });
});
