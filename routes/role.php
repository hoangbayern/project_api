<?php
use App\Http\Controllers\Admin\RoleController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin/roles')->group(function (){
    Route::controller(RoleController::class)->group(function (){
        Route::as('role.')->group(function (){
            Route::get('/list','index')->name('index')
            ->middleware('permission:role.index');
            Route::get('/active-role/{id}','active')->name('active');
            Route::get('/inactive-role/{id}','inactive')->name('inactive');
            Route::delete('/{id}','destroy')->name('destroy')
            ->middleware('permission:role.destroy');
            Route::get('/create','create')->name('create')
            ->middleware('permission:role.create');
            Route::post('/create/role','store')->name('store')
            ->middleware('permission:role.create');
            Route::get('/update/{id}','edit')->name('edit')
            ->middleware('permission:role.update');
            Route::post('/update/role/{id}','update')->name('update')
            ->middleware('permission:role.update');
            Route::get('/search-list','roleListAjax')->name('roleListAjax');
            Route::get('/search-role','search')->name('searchRole');
        });
    });
});
