<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AutheticateController;

Route::get('/admin',[AutheticateController::class, 'index'])
    ->name('admin.index')
    ->middleware('auth');
Route::get('/admin_login',[AutheticateController::class, 'login'])
    ->name('admin.login');
Route::post('/admin_login',[AutheticateController::class, 'postLogin'])
    ->name('admin.postLogin');
Route::get('/admin_register',[AutheticateController::class, 'register'])
    ->name('admin.register');
Route::post('/admin_register',[AutheticateController::class, 'postRegister'])
    ->name('admin.postRegister');
Route::get('/logout',[AutheticateController::class, 'logout'])
    ->name('admin.logout');
