<?php

use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin/users')->group(function (){
    Route::controller(UserController::class)->group(function (){
        Route::as('user.')->group(function (){
            Route::get('/list','index')->name('index')
                ->middleware('permission:user.index');
            Route::get('/active-user/{id}','active')->name('active');
            Route::get('/inactive-user/{id}','inactive')->name('inactive');
            Route::delete('/{id}','destroy')->name('destroy')
            ->middleware('permission:user.destroy');
            Route::get('/create','create')->name('create')
            ->middleware('permission:user.create');
            Route::post('/create/user','store')->name('store')
            ->middleware('permission:user.create');
            Route::get('/update/{id}','edit')->name('edit')
            ->middleware('permission:user.update');
            Route::put('update/user/{id}','update')->name('update')
            ->middleware('permission:user.update');
            Route::get('/show/{id}','show')->name('show')
            ->middleware('permission:user.show');
            Route::get('/search-list','userListAjax')->name('userListAjax');
            Route::get('/search-user','search')->name('search');
        });
    });
});
