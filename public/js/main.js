
const DEBOUNCING_TIME = 500;

function debounce(func, delay) {
    let timer;
    return function () {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, arguments);
        }, delay);
    };
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

let sendAjax = function (url, method, data) {
    return $.ajax({
        url: url,
        data: data,
        method: method,
        typeData: 'json',
        processData: false,
        contentType: false,
        async: false
    });
}

let sendAjaxNormal = function (url, method, data) {
    return $.ajax({
        url: url,
        data: data,
        method: method,
        typeData: 'json',processData: false,
        contentType: false,
        async: false
    });
}

// Notice Delete
let confirmDelete = function (){
    return Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    });
}

// Notify Delete Success
let notifyDelete = function () {
    return Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
    )
}

//
let renderError = function (errors){
    for (let key in errors){
        let inputError = $('#' + key);
        let elementError = $('#' + key + '-error');
        inputError.addClass('is-invalid');
        elementError.text(errors[key]);
    }
    const firstError = $('.is-invalid').first();
    if (firstError) {
        firstError.focus();
    }
}

//
let removeErrors = function (data){
    for (let key of data.keys()) {
        let inputError = $('#' + key);
        let elementError = $('#' + key + '-error');
        inputError.removeClass('is-invalid');
        elementError.text('');
    }
}


// Click open Form
$(document).on('click','.open-modal',function (e){
    e.preventDefault();
    let url = $(this).attr('href');
    console.log(url)
    $.get(url,function (response) {
        $('#modal').html(response.form)
        $('#modal-form').modal('show')
    },'json')
})
// Click Submit Create Form
$(document).on('click','#submit',function (e){
    e.preventDefault();
    let url = $('#form-data').attr('action');
    let data = new FormData($('#form-data')[0]);
    sendAjax(url,'POST',data).then(function (response) {
        $('#table').html(response.table)
        $('#modal-form').modal('hide')
        Swal.fire('Created!', '', 'success')
    }).fail(function (errors) {
        let errorsObj = JSON.parse(errors.responseText).errors;
        removeErrors(data)
        renderError(errorsObj)
    })
})
// Submit Update Form
$(document).on('click','#submitUpdate',function (e){
    e.preventDefault();
    let url = $('#form-data').attr('action');
    let data = new FormData($('#form-data')[0]);
    sendAjax(url,'POST',data).then(function (response) {
        $('#table').html(response.table)
        $('#modal-form').modal('hide')
        Swal.fire('Update Success!', '', 'success')
    }).fail(function (errors) {
        let errorsObj = JSON.parse(errors.responseText).errors;
        removeErrors(data)
        renderError(errorsObj)
    })
})
// Click Button Delete
$(document).on('click','.btnDelete',function (e){
    e.preventDefault();
    let url = $(this).attr('href');
    // let data = new FormData($('#form-data')[0]);
        confirmDelete().then((result) => {
            if (result.isConfirmed) {
                sendAjax(url,'DELETE').done(function (response) {
                    $('#table').html(response.table)
                    notifyDelete()
                })
            }
        })
})

// Xu ly khi nhap key vao Input Error bi Xoa
$(document).on('keypress', '.is-invalid', function (){
    $(this).removeClass('is-invalid');
})

// ListAuto Search
$( function() {
    $.ajax({
        url: '/admin/products/search-list',
        method: 'GET',
        success: function (response) {
            startAutoSearch(response)
        }
    })
    function startAutoSearch(availableTags) {
        $( "#search_product" ).autocomplete({
            source: availableTags
        });
    }
} );

$( function() {
    $.ajax({
        url: '/admin/categories/search-list',
        method: 'GET',
        success: function (response) {
            startAutoSearch(response)
        }
    })
    function startAutoSearch(availableTags) {
        $( "#search_category" ).autocomplete({
            source: availableTags
        });
    }
} );

$( function() {
    $.ajax({
        url: '/admin/users/search-list',
        method: 'GET',
        success: function (response) {
            startAutoSearch(response)
        }
    })
    function startAutoSearch(availableTags) {
        $( "#search_user" ).autocomplete({
            source: availableTags
        });
    }
} );

let listData = async function (url = null) {
    let form = $('#form-search');
    let link = url ?? form.attr('action');
    let data = form.serialize();
    await getListData(link, 'GET', data);
}

let getListData = async function (url, method, data = '') {
    await sendAjax(url, 'GET', data).then((response) => {
        $('#table').html(response.table)
    });
}
listData().catch();

$(document).on('keyup','#search_product', debounce(function() {
    listData().catch();
},DEBOUNCING_TIME));

$(document).on('keyup','#search_category', debounce(function() {
    listData().catch();
},DEBOUNCING_TIME));

$(document).on('keyup','#search_user', debounce(function() {
    listData().catch();
},DEBOUNCING_TIME));

$(document).on('keyup','#search_role', debounce(function() {
    listData().catch();
},DEBOUNCING_TIME));

$(document).on('keyup','.inputSearch', debounce(function() {
    listData().catch();
},DEBOUNCING_TIME));


$(document).on('change','#category', debounce(function() {
    listData().catch();
},DEBOUNCING_TIME));


$(document).on('click', '.pagination a', function (event) {
    event.preventDefault();
    let url = $(this).attr('href');
    listData(url).catch();
})
//
// $(document).on('change','#category',function (e){
//     e.preventDefault();
//     let url = $('#form-search').attr('action');
//     let data = new FormData($('#form-search')[0]);
//     sendAjax(url,'POST',data).then(function (response) {
//         $('#table').html(response.table)
//     })
// })
//
// $(document).on('keyup','.inputSearch',function (e){
//     e.preventDefault();
//     let url = $('#form-search').attr('action');
//     let data = new FormData($('#form-search')[0]);
//     sendAjax(url,'POST',data).then(function (response) {
//         $('#table').html(response.table)
//     })
// })


