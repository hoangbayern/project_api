<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('user.update',$user->id)}}" method="post" enctype="multipart/form-data" id="form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">UserName</label>
                        <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name">
                        <span id="name-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Image</label> <br>
                        <img src="{{ getImage($user->image) }}" alt=""
                             class="img-thumbnail img-circle">
                        <input type="file" class="form-control" name="image" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email</label>
                        <input type="text" min="0" class="form-control" value="{{$user->email}}" name="email" id="email">
                        <span id="email-error" class="error invalid-feedback"></span>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Permission</label>
                        <div class="form-check">
                            @foreach($roles as $role)
                                <input name="roles[]" type="checkbox"  value="{{$role->id}}" id="{{$role->id}}"
                                @foreach($user->roles as $item)
                                    @checked($role->id === $item->id)
                                    @endforeach
                                >
                                <label for="{{ $role->id }}">{{$role->name}}</label>
                                <br>
                            @endforeach
                        </div>
                    </div>

{{--                    <div class="form-group">--}}
{{--                        <label for="recipient-name" class="col-form-label">Roles</label>--}}
{{--                        @foreach($user->roles as $role)--}}
{{--                            <li >{{$role->name}}</li>--}}
{{--                        @endforeach--}}
{{--                        <select name="roles[]" multiple="multiple" class="form-control form-control-sm" id="roles[]">--}}
{{--                            <option value="">---->Select roles<----</option>--}}
{{--                            @foreach($roles as $role)--}}
{{--                                <option value="{{$role->id}}">{{$role->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        <span id="category-error" class="error invalid-feedback"></span>--}}
{{--                    </div>--}}
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gender</label>
{{--                        @php--}}
{{--                             if ($user->gender == 0){--}}
{{--                                 echo '<li>Male</li>';--}}
{{--                             }--}}
{{--                             else{--}}
{{--                                 echo '<li>Female</li>';--}}
{{--                             }--}}
{{--                        @endphp--}}
                        <br>
                        <select name="gender" class="form-control form-control-sm" id="exampleFormControlSelect3">
                            <option value="0" {{$user->gender == 0 ? 'selected':''}}>Male</option>
                            <option value="1" {{$user->gender == 1 ? 'selected':''}}>Female</option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submitUpdate">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



