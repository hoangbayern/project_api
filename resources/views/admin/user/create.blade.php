<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data" id="form-data">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">UserName</label>
                        <input type="text" class="form-control" value="" name="name" id="name">
                        <span id="name-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Image</label>
                        <input type="file" class="form-control" value="" name="image" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email</label>
                        <input type="text" min="0" class="form-control" value="" name="email" id="email">
                        <span id="email-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Password</label>
                        <input type="password" min="0" class="form-control" value="" name="password" id="password">
                        <span id="password-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gender</label>
                        <select name="gender" class="form-control form-control-sm" id="gender">
                                <option value="0">Male</option>
                                <option value="1">Female</option>
                        </select>
                        <span id="gender-error" class="error invalid-feedback"></span>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submit">Create</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


