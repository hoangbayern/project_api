<table class="table">
    <thead class="table table-dark">
    <tr>
        <th> ID</th>
        <th> Name</th>
        <th> Image</th>
        <th> Email</th>
        <th> Gender</th>
        <th> Activated</th>
        <th> Action</th>
    </tr>
    </thead>
    <tbody class="table table-warning">
    @if($users->isNotEmpty())
    @foreach($users as $user)
        <tr>
            <td> {{$user->id}}</td>
            <td> {{$user->name}}</td>
            <td>
                <img src="{{ getImage($user->image) }}" alt=""
                     class="img-thumbnail img-circle">
            </td>
            <td> {{$user->email}}</td>
            <td>
                @php
                     if ($user->gender == 0){
                         echo 'Male';
                     }
                     else{
                         echo 'Female';
                     }
                @endphp
            </td>
            <td>
                @php
                    if ($user->activated == 0){
                @endphp
                <a href="{{route('user.active',$user->id)}}" onclick="javascript:void(0)" class="btnActive"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                @php
                    }else{
                @endphp
                <a href="{{route('user.inactive',$user->id)}}" onclick="javascript:void(0)" class="btnActive"><span class="fa-thumb-styling fa fa-thumbs-up" ></span></a>

                @php
                    }
                @endphp
            </td>
            <td>
                @hasPermission('user.show')
                <a href="{{route('user.show',$user->id)}}" class="open-modal" onclick="javascript:void(0) " title="View Student">
                    <button class="btn btn-info btn-sm btn-info" data-toggle="modal">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                </a>
                @endhasPermission

                @hasPermission('user.update')
                <a href="{{route('user.edit',$user->id)}}" class="open-modal" onclick="javascript:void(0) " title="View Student">
                    <button class="btn btn-info btn-sm btn-secondary" data-toggle="modal">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                </a>
                @endhasPermission

                @hasPermission('user.destroy')
                <a href="{{route('user.destroy',$user->id)}}" onclick="javascript:void(0)" class="btnDelete" title="View Student">
                    <button class="btn btn-info btn-sm btn-danger btn-open-modal" data-toggle="modal" id="submitDelete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </a>
                @endhasPermission
            </td>
        </tr>
    @endforeach
    @else
        <tr>
            <td colspan="7" style="text-align: center; color: red; font-size: 2rem">Not Found Data</td>
        </tr>
    @endif
    </tbody>
</table>
{{$users->links()}}

