<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Show User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">UserName</label>
                        <input type="text" class="form-control" value="{{$user->name}}" name="name" id="recipient-name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Image</label> <br>
                        <img src="{{ getImage($user->image) }}" alt=""
                             class="img-thumbnail img-circle">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email</label>
                        <input type="text" min="0" class="form-control" value="{{$user->email}}" name="email" id="recipient-name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gender</label>
                        @php
                            if ($user->gender == 0){
                                echo '<li>Male</li>';
                            }
                            else{
                                echo '<li>Female</li>';
                            }
                        @endphp
                        <br>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>




