
@extends('layouts.app')

@section('content')
    @hasPermission('user.index')
    <div class="content-wrapper">
        <div class="form-group">
            <form onsubmit="return false;" action="{{route('user.search')}}" method="GET" enctype="multipart/form-data" id="form-search">
                @csrf
                @method('GET')
                <div class="input-group">
                    <input type="search" id="search_user" name="name" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                </div>
            </form>
        </div>
        <div class="page-header">
            <h3 class="page-title"> List User </h3>
            @hasPermission('user.create')
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <button type="button" class="btn btn-primary open-modal" onclick="javascript:void(0)" href="{{route('user.create')}}">Create</button>
{{--                    <a class="open-modal" onclick="javascript:void(0)" href="{{route('user.create')}}">Create</a>--}}
                </ol>
            </nav>
            @endhasPermission
        </div>

        <div id="table">
            @include('admin.user.paginate')
        </div>

        <div id="modal"></div>

    </div>
    @endhasPermission
@endsection

