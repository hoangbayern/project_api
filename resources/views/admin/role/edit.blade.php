<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('role.update',$role->id)}}" method="post" id="form-data">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name Role</label>
                        <input type="text" class="form-control" value="{{$role->name}}" name="name" id="name">
                        <span id="name-error" class="error invalid-feedback"></span>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Permission</label>
                        <div class="form-check">
                            @foreach($permissions as $item)
                                <input name="permissions[]" type="checkbox"  value="{{$item->id}}" id="{{$item->id}}"
                                      @foreach($role->permissions as $permission)
                                          @checked($item->id === $permission->id)
                                      @endforeach
                                >
                                <label for="{{ $item->id }}">{{$item->name}}</label>
                                <br>
                            @endforeach
                        </div>
                    </div>

{{--                    <div class="form-group">--}}
{{--                        <label for="recipient-name" class="col-form-label">Permission</label>--}}
{{--                        @foreach($role->permissions as $item)--}}
{{--                            <li >{{$item->name}}</li>--}}
{{--                        @endforeach--}}
{{--                        <select name="permissions[]" multiple="multiple" class="form-control form-control-sm" id="permissions[]">--}}
{{--                            @foreach($permissions as $item)--}}
{{--                                <option value="{{$item->id}}">{{$item->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        <span id="category-error" class="error invalid-feedback"></span>--}}
{{--                    </div>--}}
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submitUpdate">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


