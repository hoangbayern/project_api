@extends('layouts.app')

@section('content')
    @hasPermission('role.index')
    <div class="content-wrapper">
        <div class="form-group">
            <form onsubmit="return false" action="{{route('role.searchRole')}}" method="get" id="form-search">
                @csrf
                @method('GET')
                <div class="input-group">
                    <label>Search User</label>
                    <br>
                    <input type="search" id="search_role" name="name" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                </div>
            </form>
        </div>
        <div class="page-header">
            <h3 class="page-title"> List Role </h3>
            @hasPermission('role.create')
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <button type="button" class="btn btn-primary open-modal" onclick="javascript:void(0)" href="{{route('role.create')}}">Create</button>
                    {{--                    <a class="open-modal" onclick="javascript:void(0) " href="{{route('category.create')}}">Create</a>--}}
                </ol>
            </nav>
            @endhasPermission
        </div>

        <div id="table">
            @include('admin.role.paginate')
        </div>
        <div id="modal"></div>
    </div>
    @endhasPermission()
@endsection
