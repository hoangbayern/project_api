<table class="table">
    <thead class="table table-dark">
    <tr>
        <th> ID</th>
        <th> Name</th>
        <th> Permission</th>
        <th> Activated</th>
        <th> Action</th>
    </tr>
    </thead>
    <tbody class="table table-warning">
    @foreach($roles as $role)
    <tr>
        <td> {{$role->id}}</td>
        <td> {{$role->name}}</td>
        <td>
            @foreach($role->permissions as $permission)
                {{$permission->name}} <br>
            @endforeach
        </td>
        <td>
            @php
                if ($role->activated == 0){
            @endphp
            <a href="{{route('role.active',$role->id)}}"><span class="fa-thumb-styling fa fa-thumbs-down" id="active"></span></a>
            @php
                }else{
            @endphp
            <a href="{{route('role.inactive',$role->id)}}"><span class="fa-thumb-styling fa fa-thumbs-up" id="inactive"></span></a>

            @php
                }
            @endphp
        </td>
        <td>
            <a href="#" class="open-modal" onclick="javascript:void(0) " title="View Student">
                <button class="btn btn-info btn-sm btn-info" data-toggle="modal">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </button>
            </a>
            @hasPermission('role.update')
            <a href="{{route('role.edit',$role->id)}}" class="open-modal" onclick="javascript:void(0) " title="View Student">
                <button class="btn btn-info btn-sm btn-secondary" data-toggle="modal">
                    <i class="fa fa-edit" aria-hidden="true"></i>
                </button>
            </a>
            @endhasPermission

            @hasPermission('role.destroy')
            <a href="{{route('role.destroy',$role->id)}}" onclick="javascript:void(0)" class="btnDelete" title="View Student">
                <button class="btn btn-info btn-sm btn-danger btn-open-modal" data-toggle="modal" id="submitDelete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
            </a>
            @endhasPermission
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

{{$roles->links()}}

