<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Show Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name Product</label>
                        <input type="text" class="form-control" value="{{$product->name}}" name="name" id="recipient-name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Image</label> <br>
                        <img src="{{ getImage($product->image) }}" alt=""
                             class="img-thumbnail img-circle">
                          </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Stock Product</label>
                        <input type="number" min="0" class="form-control" value="{{$product->stock}}" name="stock" id="recipient-name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Price Product</label>
                        <input type="text" class="form-control" value="{{number_format($product->price)}}" name="price" id="recipient-name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Color</label>
                        <input type="text" class="form-control" value="{{$product->list_color}}" name="list_color" id="price" disabled>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gender</label>
                        <br>
                        @php
                            if ($product->gender == 0){
                                echo '<li>Nam</li>';
                            }
                            else{
                                echo '<li>Nữ</li>';
                            }
                        @endphp
                        <br>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Description</label>
                        <input type="text" class="form-control" value="{{$product->description}}" name="description" id="recipient-name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Category</label>
                        <select name="categories[]" multiple="multiple" class="form-control form-control-sm" id="exampleFormControlSelect3" name="" disabled>
                            @foreach($product->categories as $item)
                                <option >{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



