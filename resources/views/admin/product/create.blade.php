<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data" id="form-data">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name Product</label>
                        <input type="text" class="form-control" value="" name="name" id="name">
                        <span id="name-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Image</label>
                        <input type="file" class="form-control" value="" name="image" id="image_product">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Stock Product</label>
                        <input type="number" min="0" class="form-control" value="" name="stock" id="stock">
                        <span id="stock-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Price Product</label>
                        <input type="text" class="form-control" value="" name="price" id="price">
                        <span id="price-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Color</label>
                        <input type="text" class="form-control" value="" name="list_color" id="price">
                        <span id="list_color-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gender</label>
                        <select name="gender" class="form-control form-control-sm" id="gender">
                            <option value="0">Nam</option>
                            <option value="1">Nữ</option>
                        </select>
                        <span id="gender-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Description</label>
                        <input type="text" class="form-control" value="" name="description" id="description">
                        <span id="description-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Category</label>
                        <select name="categories[]" multiple="multiple" class="form-control form-control-sm" id="categories[]" name="">
                            @foreach($categories as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <span id="category-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submit">Create</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


