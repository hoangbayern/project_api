<table class="table">
    <thead class="table table-dark">
    <tr>
        <th> ID</th>
        <th> Name</th>
        <th> Image</th>
        <th> Stock</th>
        <th> Price</th>
        <th> Color</th>
        <th> Gender</th>
        <th> Activated</th>
        <th> Action</th>
    </tr>
    </thead>
    <tbody class="table table-warning">
    @if($products->isNotEmpty())
    @foreach($products as $product)
        <tr>
            <td> {{$product->id}}</td>
            <td> {{$product->name}}</td>
            <td>
                <img src="{{ getImage($product->image) }}" alt="">
            </td>
            <td> {{$product->stock}}</td>
            <td> {{number_format($product->price).' '.'VNĐ'}}</td>
            <td> {{$product->list_color}}</td>
            <td>
                @php
                    if ($product->gender == 0){
                        echo 'Nam';
                    }
                    else{
                        echo 'Nữ';
                    }
                @endphp
            </td>
            <td>
                @php
                    if ($product->activated == 0){
                @endphp
                <a href="{{route('product.active',$product->id)}}"><span class="fa-thumb-styling fa fa-thumbs-down" id="active"></span></a>
                @php
                    }else{
                @endphp
                <a href="{{route('product.inactive',$product->id)}}"><span class="fa-thumb-styling fa fa-thumbs-up" id="inactive"></span></a>

                @php
                    }
                @endphp
            </td>
            <td>
                @hasPermission('product.show')
                <a href="{{route('product.show',$product->id)}}" class="open-modal" onclick="javascript:void(0) " title="View Student">
                    <button class="btn btn-info btn-sm btn-info" data-toggle="modal">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                </a>
                @endhasPermission

                @hasPermission('product.update')
                <a href="{{route('product.edit',$product->id)}}" class="open-modal" onclick="javascript:void(0) " title="View Student">
                    <button class="btn btn-info btn-sm btn-secondary" data-toggle="modal">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                </a>
                @endhasPermission

                @hasPermission('product.destroy')
                <a href="{{route('product.destroy',$product->id)}}" onclick="javascript:void(0)" class="btnDelete" title="View Student">
                    <button class="btn btn-info btn-sm btn-danger btn-open-modal" data-toggle="modal" id="submitDelete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </a>
                @endhasPermission
            </td>
        </tr>
    @endforeach
    @else
        <tr>
            <td colspan="7" style="text-align: center; color: red; font-size: 2rem">Not Found Data</td>
        </tr>
    @endif
    </tbody>
</table>
{{$products->links()}}
