
@extends('layouts.app')

@section('content')
    @hasPermission('product.index')
    <div class="content-wrapper">
        <div class="search-bar">
        <div class="form-group">
            <form action="{{route('product.searchProduct')}}" method="get" enctype="multipart/form-data" id="form-search">
                @csrf
                @method('GET')
                <div>
                    <label>Price From</label>
                    <input type="text" name="price_from" class="inputSearch">
                    <label>Price To</label>
                    <input type="text" name="price_to" class="inputSearch">
                </div>
                <div class="category-group">
                    <label for="recipient-name" class="col-form-label">Category</label>
                    <select class="form-control form-control-sm" id="category" name="parent_id">
                        <option value="">------>Select Category<------</option>
                        @foreach($categories as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group">
                    <input type="search" id="search_product" name="product_name" class="form-control inputSearch" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                </div>
            </form>
        </div>
        </div>

        <div class="page-header">
            <h3 class="page-title"> List Product </h3>
            @hasPermission('product.create')
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <button type="button" class="btn btn-primary open-modal" onclick="javascript:void(0)" href="{{route('product.create')}}">Create</button>
{{--                    <a class="open-modal" onclick="javascript:void(0)" href="{{route('product.create')}}">Create</a>--}}
                </ol>
            </nav>
            @endhasPermission
        </div>

        <div id="table">
            @include('admin.product.paginate')
        </div>

        <div id="modal"></div>

    </div>
    @endhasPermission
@endsection

