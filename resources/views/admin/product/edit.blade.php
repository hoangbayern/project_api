<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('product.update',$product->id)}}" method="post" enctype="multipart/form-data" id="form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name Product</label>
                        <input type="text" class="form-control" value="{{$product->name}}" name="name" id="name">
                        <span id="name-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Image</label> <br>
                        <img src="{{ getImage($product->image) }}" alt=""
                             class="img-thumbnail img-circle">
                        <input type="file" class="form-control" value="" name="image" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Stock Product</label>
                        <input type="number" min="0" class="form-control" value="{{$product->stock}}" name="stock" id="stock">
                        <span id="stock-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Price Product</label>
                        <input type="text" class="form-control" value="{{$product->price}}" name="price" id="price">
                        <span id="price-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Color</label>
                        <input type="text" class="form-control" value="{{$product->list_color}}" name="list_color" id="price">
                        <span id="list_color-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gender</label>
                        <br>
                        <select name="gender" class="form-control form-control-sm" id="exampleFormControlSelect3">
                            <option value="0" {{$product->gender == 0 ? 'selected':''}}>Nam</option>
                            <option value="1" {{$product->gender == 1 ? 'selected':''}}>Nữ</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Description</label>
                        <input type="text" class="form-control" value="{{$product->description}}" name="description" id="description">
                        <span id="description-error" class="error invalid-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Category</label>
{{--                        @foreach($product->categories as $item)--}}
{{--                            <li >{{$item->name}}</li>--}}
{{--                        @endforeach--}}
                        <select name="categories[]" multiple="multiple" class="form-control form-control-sm" id="exampleFormControlSelect3">
                            @foreach($products as $item)
                                <option value="{{ $item->id }}"
                                @foreach($product->categories as $cate)
                                    {{$cate->id === $item->id ? 'selected' : ''}}
                                    @endforeach
                                >
                                    {{ $item->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submitUpdate">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>




