
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('category.update',$category->id)}}" method="post" id="form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name Category</label>
                        <input type="text" class="form-control" value="{{$category->name}}" name="name" id="name">
                        <span id="name-error" class="error invalid-feedback" style="color: red"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submitUpdate">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


