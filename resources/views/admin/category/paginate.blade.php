<table class="table">
    <thead class="table table-dark">
    <tr>
        <th> ID</th>
        <th> Name</th>
        <th> Activated</th>
        <th> Action</th>
    </tr>
    </thead>
    <tbody class="table table-warning">
    @if($categories->isNotEmpty())
        @foreach($categories as $category)
            <tr>
                <td> {{$category->id}}</td>
                <td> {{$category->name}}</td>
                <td>
                    @php
                        if ($category->activated == 0){
                    @endphp
                    <a href="{{route('category.active',$category->id)}}"><span
                            class="fa-thumb-styling fa fa-thumbs-down" id="active"></span></a>
                    @php
                        }else{
                    @endphp
                    <a href="{{route('category.inactive',$category->id)}}"><span
                            class="fa-thumb-styling fa fa-thumbs-up" id="inactive"></span></a>

                    @php
                        }
                    @endphp
                </td>
                <td>
                    <a href="#" title="View Student">
                        <button class="btn btn-info btn-sm btn-info" data-toggle="modal">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </button>
                    </a>
                    @hasPermission('category.update')
                    <a href="{{route('category.edit',$category->id)}}" class="open-modal" onclick="javascript:void(0) " title="View Student">
                        <button  class="btn btn-info btn-sm btn-secondary" data-toggle="modal">
                            <i class="fa fa-edit" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endhasPermission

                    @hasPermission('category.destroy')
                    <a href="{{route('category.destroy',$category->id)}}" onclick="javascript:void(0)" class="btnDelete">
                        <button class="btn btn-info btn-sm btn-danger" data-toggle="modal" id="submitDelete">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="5" style="text-align: center; color: red; font-size: 2rem">Not Found Data</td>
        </tr>
    @endif
    </tbody>

</table>

{{$categories->links()}}
