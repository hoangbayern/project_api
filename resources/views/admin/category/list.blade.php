@extends('layouts.app')

@section('content')
    @hasPermission('category.index')
    <div class="content-wrapper">
        <div class="form-group">
            <form onsubmit="return false;" action="{{route('category.searchCategory')}}" method="get" enctype="multipart/form-data" id="form-search">
                @csrf
                @method('GET')
                <div class="input-group">
                    <label for="recipient-name" class="col-form-label">Search Category</label>
                    <input type="search" id="search_category" name="name" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                </div>
            </form>
        </div>
        <div class="page-header">
            <h3 class="page-title"> List Category </h3>
            @hasPermission('category.create')
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <button type="button" class="btn btn-primary open-modal" onclick="javascript:void(0)" href="{{route('category.create')}}">Create</button>
{{--                    <a class="open-modal" onclick="javascript:void(0) " href="{{route('category.create')}}">Create</a>--}}
                </ol>
            </nav>
            @endhasPermission()
        </div>

         <div id="table">
             @include('admin.category.paginate')
         </div>
        <div id="modal"></div>
    </div>
@endhasPermission
@endsection
