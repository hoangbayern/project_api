<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait HandleImage
{
    public string $path;
    public string $disk = 'image';

    public function verify($request): bool
    {
        return $request->hasFile('image');
    }


    public function saveImage($request): string|null
    {
        if ($this->verify($request)) {
            $image = $request->image;
            $typeIMG = $image->getClientOriginalExtension();
            $image = Image::make($image)->encode($typeIMG);
            $name = time().'.'.$typeIMG;
            $fullPath = $this->path.$name;
            Storage::disk($this->disk)->put($fullPath, $image);
            return $name;
        }
        return null;
    }

    public function updateImage($request, $currentImage): string
    {
        if ($this->verify($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function imageExists($name): bool|string
    {
        $name = str_replace('images/', '', $name);
        if (Storage::disk($this->disk)->exists($name) && !empty($name)) {
            return $name;
        }
        return false;
    }

    public function deleteImage($name): bool
    {
        $name = str_replace('images/', '', $name);
        return Storage::disk($this->disk)->delete($name);
    }

    /**
     * @param  string  $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path.'/';
    }

    /**
     * @param  string  $disk
     */
    public function setDisk(string $disk): void
    {
        $this->disk = $disk;
    }


}
