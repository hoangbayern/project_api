<?php

namespace App\Models;

use Dotenv\Util\Str;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const PATH_IMAGE = 'images/products/';
    protected $table = 'products';

    protected $fillable = [
        'name',
        'image',
        'description',
        'stock',
        'price',
        'list_color',
        'gender',
        'activated'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product',
            'product_id', 'category_id');
    }

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn($image) =>
            (empty($image)) ? null : ((\Illuminate\Support\Str::of($image)->isMatch('/http(.*)/')) ?  $image : self::PATH_IMAGE . $image)
        );

    }


    public function scopeWithName($query, $name)
    {
        return empty($name) ? $query : $query->where('name', $name);
    }

    public function scopeWithCategory($query, $id)
    {
        return empty($id) ? $query : $query->whereHas('categories', fn($query) => $query->where('category_id', $id)
        );
    }

    public function scopeWithPriceFrom($query, $price)
    {
        return empty($price) ? $query : $query->where('price', '>=',$price);
    }

    public function scopeWithPriceTo($query, $price)
    {
        return empty($price) ? $query : $query->where('price','<=', $price);
    }
}
