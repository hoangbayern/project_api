<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';
    protected $fillable = [
        'name',
        'activated'
    ];

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class,'roles_permissions','role_id','permission_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'roles_users', 'role_id', 'user_id');
    }

    public function hasPermission($permission)
    {
        return $this->permissions()->pluck('name')->contains($permission);
    }

    public function scopeWithName($query, $name)
    {
        return (empty($name)) ? $query : $query->where('name', 'LIKE', '%' . $name . '%');
    }

}
