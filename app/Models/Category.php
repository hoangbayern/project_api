<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'activated'
    ];

//    public function parent()
//    {
//        return $this->belongsTo(Category::class, 'parent_id')->withDefault();
//    }

//    public function subCategories()
//    {
//        return $this->hasMany(Category::class, 'parent_id');
//    }

//    public function getParents()
//    {
//        return Category::whereNull('parent_id')->get(['id', 'name']);
//    }

    public function scopeWithName($query, $name)
    {
        return empty($name) ? $query : $query->where('name', $name);
    }

//    public function scopeWithParentName($query, $name)
//    {
//        return empty($name) ? $query : $query->orWhereHas('parent', fn($query) => $query->whereFullText('name', $name)
//        );
//    }
}
