<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;


    const ROLE_ADMIN = 'Admin';
    const PATH_IMAGE = 'images/users/';
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'image',
        'email',
        'password',
        'gender',
        'activated'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn($image) =>
            (empty($image)) ? null : ((\Illuminate\Support\Str::of($image)->isMatch('/http(.*)/')) ?  $image : self::PATH_IMAGE . $image)
        );

    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'roles_users', 'user_id', 'role_id');
    }

    public function hasPermission($permission): bool
    {
        if ($this->isAdmin()){
            return true;
        }
        foreach ($this->roles as $role){
            if ($role->hasPermission($permission)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($name)
    {
        return $this->roles->pluck('name')->contains($name);
    }

    public function isAdmin(){
        return $this->hasRole(self::ROLE_ADMIN);
    }


    public function scopeOrWithName($query, $name)
    {
        return empty($name) ? $query : $query->orWhereFullText('name', $name);
    }

    public function scopeOrWithEmail($query, $email)
    {
        return empty($email) ? $query : $query->orWhereFullText('email', $email);
    }

}
