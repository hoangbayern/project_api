
<?php


    function getImage($url): string
    {
        if (empty($url)){
            return asset('');
        }
        if (strpos('http', $url)){
            return $url;
        }
        return asset($url);
    }


