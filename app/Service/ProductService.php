<?php

namespace App\Service;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;

class ProductService
{
    use HandleImage;

    public string $pathProduct = 'products';
    public ProductRepository $productRepository;
    public CategoryRepository $categoryRepository;

    /**
     * @param  ProductRepository  $productRepository
     * @param  CategoryRepository  $categoryRepository
     */
    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->setPath($this->pathProduct);
    }


    public function index()
    {
        return $this->productRepository->latest('id')->paginate(10);
    }


    public function create(Request $request)
    {
        $dataProduct = $request->all();
        $dataProduct['image'] = $this->saveImage($request);
        $categories = $request->categories;
        $product = $this->productRepository->create($dataProduct);
        $product->categories()->attach($categories);
        return $product;
    }

    public function update(Request $request, $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $dataProduct = $request->all();
        $dataProduct['image'] = $this->updateImage($request, $product->image);
        $categories = $request->categories;
        $this->productRepository->update($dataProduct, $id);
        $product->categories()->sync($categories);
        return $product;
    }

    public function edit($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function show($id)
    {
        return $this->productRepository->showOne($id);
    }

    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        $this->deleteImage($product->image);
        return $this->productRepository->delete($id);
    }

    public function active($id)
    {
        return $this->productRepository->active($id);
    }

    public function inactive($id)
    {
        return $this->productRepository->inactive($id);
    }

    public function getAllProduct()
    {
        return $this->productRepository->all();
    }

    public function productListAjax()
    {
        $products = $this->productRepository->all();
        $data = [];
        foreach ($products as $item) {
            $data[] = $item['name'];
        }
        return $data;
    }

    public function search($array)
    {
        return $this->productRepository->search($array->all());
    }

}
