<?php
namespace App\Service;

use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    public RoleRepository $roleRepository;
    public PermissionRepository $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        return $this->roleRepository->latest('id')->paginate(10)
            ->withPath(route('role.index'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $role = $this->roleRepository->create($request->except('permissions'));
        $permissions = $request->permissions;
        $role->permissions()->attach($permissions);
        return $role;
    }

    public function edit($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $dataRole = $request->except('permissions');
        $permissions = $request->permissions;
        $role = $this->roleRepository->update($dataRole,$id);
        $role->permissions()->sync($permissions);
        return $role;
    }

    public function delete($id)
    {
        return $this->roleRepository->delete($id);
    }

    public function active($id){
        return $this->roleRepository->active($id);
    }

    public function inactive($id){
        return $this->roleRepository->inactive($id);
    }

    public function getAllRole(){
        return $this->roleRepository->all();
    }

    public function roleListAjax()
    {
        $roles = $this->roleRepository->all();
        $data = [];
        foreach ($roles as $item) {
            $data[] = $item['name'];
        }
        return $data;
    }
    public function search($name){
        return $this->roleRepository->search($name);
    }
}

