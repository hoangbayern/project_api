<?php
namespace App\Service;

use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    use HandleImage;
    public string $pathUser = 'users';
    public UserRepository $userRepository;
    public RoleRepository $roleRepository;

    /**
     * @param  UserRepository  $userRepository
     * @param  RoleRepository  $roleRepository
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->setPath($this->pathUser);
    }


    public function index()
    {
        return $this->userRepository->latest('id')->paginate(10)
            ->withPath(route('user.index'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $dataUser['name'] = $request->name;
        $dataUser['email'] = $request->email;
        $dataUser['password'] = Hash::make($request->password);
        $dataUser['gender'] = $request->gender;
        if ($request->hasFile('image')){
            $name = $this->saveImage($request->image);
            $dataUser['image'] = $name;
        }
        $user = $this->userRepository->create($dataUser);
        return $user;
    }

    public function update(Request $request, $id)
    {
        $userId = $this->userRepository->findOrFail($id);
        $dataUser['name'] = $request->name;
        $dataUser['email'] = $request->email;
        $dataUser['gender'] = $request->gender;
        if ($request->hasFile('image')){
            $name = $this->updateImage($request->image,$userId->image);
            $dataUser['image'] = $name;
        }
        $roles = $request->roles;
        $user = $this->userRepository->update($dataUser,$id);
        $user->roles()->sync($roles);
        return $user;
    }

    public function edit($id){
        return $this->userRepository->findOrFail($id);
    }

    public function show($id){
        return $this->userRepository->findOrFail($id);
    }

    public function delete($id)
    {
        $user = $this->userRepository->find($id);
        $this->deleteImage($user->image);
        return $this->userRepository->delete($id);
    }

    public function active($id){
        return $this->userRepository->active($id);
    }

    public function inactive($id){
        return $this->userRepository->inactive($id);
    }

    public function getAllUser(){
        return $this->userRepository->all();
    }

    public function userListAjax()
    {
        $users = $this->userRepository->all();
        $data = [];
        foreach ($users as $item) {
            $data[] = $item['name'];
        }
        return $data;
    }

    public function search($name){
        return $this->userRepository->search($name);
    }
}

