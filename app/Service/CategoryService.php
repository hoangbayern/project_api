<?php
namespace App\Service;

use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryService
{
    public CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->latest('id')->paginate(10);
        $allCategories = $this->categoryRepository->all();
        return view('admin.category.list',compact('categories','allCategories'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->categoryRepository->create($request->all());
        return $this->categoryRepository->latest('id')->paginate(10);
    }

    public function update(Request $request, $id)
    {
        $this->categoryRepository->update($request->all(),$id);
        return $this->categoryRepository->latest('id')->paginate(10);
    }

    public function edit($id){
         return $this->categoryRepository->findOrFail($id);
    }

    public function show($id){
        return $this->categoryRepository->showOne($id);
    }

    public function delete($id)
    {
        $this->categoryRepository->delete($id);
        return $this->categoryRepository->latest('id')->paginate(10);
    }

    public function active($id){
       return $this->categoryRepository->active($id);
    }

    public function inactive($id){
        return $this->categoryRepository->inactive($id);
    }

    public function getAllCategory(){
        return $this->categoryRepository->all();
    }

    public function categoryListAjax()
    {
        $categories = $this->categoryRepository->all();
        $data = [];
        foreach ($categories as $item) {
            $data[] = $item['name'];
        }
        return $data;
    }

    public function search($name){
        return $this->categoryRepository->search($name);
    }

}
