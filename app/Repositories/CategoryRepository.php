<?php
namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search($name){
        return $this->model->withName($name)
            ->latest('id')->paginate(10)->withPath(route('category.searchCategory'))->withQueryString();
    }
}
