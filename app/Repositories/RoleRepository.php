<?php
namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository{
    public function model()
    {
        return Role::class;
    }

    public function search($name){
        return $this->model->withName($name)->latest('id')->paginate(10);
    }
}
