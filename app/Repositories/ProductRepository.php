<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository{
    public function model()
    {
        return Product::class;
    }

    public function search($array){
        return $this->model->withName($array['product_name'] ?? null)->withCategory($array['parent_id'] ?? null)->withPriceFrom($array['price_from'] ?? null )
            ->withPriceTo($array['price_to'] ?? null )->latest('id')->paginate(10)->withPath(route('product.searchProduct'))->withQueryString();
    }
}
