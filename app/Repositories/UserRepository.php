<?php
namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository{
    public function model()
    {
        return User::class;
    }

    public function search($name){
        return $this->model->orWithName($name)->orWithEmail($name)->latest('id')->paginate(10);
    }
}
