<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $permission
     * @return Response
     */
    public function handle(Request $request, Closure $next, $permission): Response
    {
        $user = Auth::user();
        if ($user->hasPermission($permission)){
            return $next($request);
        }
        return abort(403)->withErrors([
            'Warning' => 'This action is unauthorized.',
        ]);
    }
}
