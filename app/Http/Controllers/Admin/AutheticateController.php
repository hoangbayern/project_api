<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginAuthRequest;
use App\Http\Requests\RegisterAuthRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AutheticateController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.authenticate.admin_home');
    }

    public function login()
    {
        if(Auth::check()){
            return redirect('/admin');
        }
        return view('admin.authenticate.admin_login');
    }

    public function postLogin(LoginAuthRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('admin');
        }

        return redirect('/admin_login')->with('message', 'Login details are not valid!');
    }

    public function register()
    {
        if (Auth::check()){
            return redirect()->intended('admin');
        }
        return view('admin.authenticate.admin_register');
    }

    public function postRegister(RegisterAuthRequest $request)
    {
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);
        return redirect('/admin_login');
    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return redirect('/admin_login');
    }
}
