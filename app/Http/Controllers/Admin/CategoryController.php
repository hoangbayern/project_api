<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreRequest;
use App\Http\Requests\Categories\UpdateRequest;
use App\Service\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected CategoryService $categoryService;

    /**
     * @param $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $this->categoryService->index();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return response()->json([
            'form' => view('admin.category.create')->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $categories = $this->categoryService->store($request);
        return response()->json([
            'table' => view('admin.category.paginate',compact('categories'))->render()
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $category = $this->categoryService->show($id);
        return view('admin.category.list',compact($category));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $category = $this->categoryService->edit($id);
        $allCategories = $this->categoryService->getAllCategory();
        return response()->json([
            'form' => view('admin.category.editModal',compact('category','allCategories'))->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $categories = $this->categoryService->update($request,$id);
        return response()->json([
            'table' => view('admin.category.paginate',compact('categories'))->render()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $categories = $this->categoryService->delete($id);
        return response()->json([
            'table' => view('admin.category.paginate',compact('categories'))->render()
        ]);
    }

    public function active(string $id){
         $this->categoryService->active($id);
         return redirect()->route('category.index');
    }

    public function inactive(string $id){
        $this->categoryService->inactive($id);
        return redirect()->route('category.index');
    }

    public function categoryListAjax()
    {
        return $this->categoryService->categoryListAjax();
    }

    public function search(Request $request){
        $categories = $this->categoryService->search($request->name);
        return response()->json([
            'table' => view('admin.category.paginate',compact('categories'))->render()
        ]);
    }
}
