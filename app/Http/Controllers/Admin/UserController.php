<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreRequest;
use App\Http\Requests\Users\UpdateRequest;
use App\Service\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    protected UserService $userService;

    /**
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = $this->userService->index();
        return view('admin.user.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return response()->json([
            'form' => view('admin.user.create')->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $this->userService->store($request);
        $users = $this->userService->index();
        return response()->json([
            'table' => view('admin.user.paginate', compact('users'))->render()
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = $this->userService->show($id);
        return response()->json([
            'form' => view('admin.user.show',compact('user'))->render()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user= $this->userService->edit($id);
        $roles = $this->userService->roleRepository->latest('id')->get();
        return response()->json([
            'form' => view('admin.user.edit',compact('user','roles'))->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $this->userService->update($request,$id);
        $users = $this->userService->index();
        return response()->json([
            'table' => view('admin.user.paginate', compact('users'))->render()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->userService->delete($id);
        $users = $this->userService->index();
        return response()->json([
            'table' => view('admin.user.paginate', compact('users'))->render()
        ]);
    }

    public function active(string $id)
    {
        $this->userService->active($id);
        return redirect()->route('user.index');
    }

    public function inactive(string $id)
    {
        $this->userService->inactive($id);
        return redirect()->route('user.index');
    }

    public function userListAjax()
    {
        return $this->userService->userListAjax();
    }

    public function search(Request $request){
        $users = $this->userService->search($request->name);

        return response()->json([
            'table' => view('admin.user.paginate',compact('users'))->render()
        ]);
    }
}
