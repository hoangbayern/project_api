<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Roles\StoreRequest;
use App\Http\Requests\Roles\UpdateRequest;
use App\Service\RoleService;
use Illuminate\Http\Request;
use function Symfony\Component\Translation\t;

class RoleController extends Controller
{
    protected RoleService $roleService;

    /**
     * @param  RoleService  $roleService
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }


    public function index(){
        $roles = $this->roleService->index();
        return view('admin.role.list',compact('roles'));
    }

    public function create(){
        $permissions = $this->roleService->permissionRepository->all();
        return response()->json([
            'form' => view('admin.role.create',compact('permissions'))->render()
        ]);
    }

    public function store(StoreRequest $request){
        $this->roleService->store($request);
        $roles = $this->roleService->index();
        return response()->json([
            'table' => view('admin.role.paginate',compact('roles'))->render()
        ]);
    }

    public function edit(string $id)
    {
        $role = $this->roleService->edit($id);
        $permissions = $this->roleService->permissionRepository->all();
        return response()->json([
            'form' => view('admin.role.edit', compact('role', 'permissions'))->render()
        ]);
    }

    public function update(UpdateRequest $request,string $id)
    {
        $this->roleService->update($request,$id);
        $roles = $this->roleService->index();
        return response()->json([
            'table' => view('admin.role.paginate', compact('roles'))->render()
        ]);
    }

    public function destroy(string $id)
    {
        $this->roleService->delete($id);
        $roles = $this->roleService->index();
        return response()->json([
            'table' => view('admin.role.paginate', compact('roles'))->render()
        ]);
    }

    public function active(string $id)
    {
        $this->roleService->active($id);
        return redirect()->route('role.index');
    }

    public function inactive(string $id)
    {
        $this->roleService->inactive($id);
        return redirect()->route('role.index');
    }

    public function categoryListAjax()
    {
        return $this->roleService->roleListAjax();
    }

    public function search(Request $request){
        $roles = $this->roleService->search($request->name);
        return response()->json([
            'table' => view('admin.role.paginate',compact('roles'))->render()
        ]);
    }
}
