<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Service\CategoryService;
use App\Service\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected ProductService $productService;
    protected CategoryService $categoryService;

    /**
     * @param  ProductService  $productService
     * @param  CategoryService  $categoryService
     */
    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = $this->categoryService->getAllCategory();
        $products = $this->productService->index();
        return view('admin.product.list', compact(['products','categories']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = $this->categoryService->getAllCategory();
        return response()->json([
            'form' => view('admin.product.create', compact('categories'))->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $this->productService->create($request);
        $products = $this->productService->index();
        return response()->json([
            'table' => view('admin.product.paginate', compact('products'))->render()
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = $this->productService->show($id);
        return response()->json([
            'form' => view('admin.product.show', compact('product'))->render()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = $this->productService->edit($id);
        $products = $this->categoryService->getAllCategory();
        return response()->json([
            'form' => view('admin.product.edit', compact('product', 'products'))->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $this->productService->update($request, $id);
        $products = $this->productService->index();
        return response()->json([
            'table' => view('admin.product.paginate', compact('products'))->render()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->productService->delete($id);
        $products = $this->productService->index();
        return response()->json([
            'table' => view('admin.product.paginate', compact('products'))->render()
        ]);
    }

    public function active(string $id)
    {
        $this->productService->active($id);
        return redirect()->route('product.index');
    }

    public function inactive(string $id)
    {
        $this->productService->inactive($id);
        return redirect()->route('product.index');
    }

    public function productListAjax()
    {
        return $this->productService->productListAjax();
    }

    public function search(Request $request)
    {
        //SEARCH VS MULTIOPTION
        $products = $this->productService->search($request);
        return response()->json([
            'table' => view('admin.product.paginate',compact('products'))->render()
        ]);
    }

}
