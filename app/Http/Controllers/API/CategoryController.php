<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreRequest;
use App\Http\Requests\Categories\UpdateRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    protected Category $category;

    /**
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = $this->category->paginate(5);
        $categoriesCollection = CategoryResource::collection($categories)->response()->getData(true);
        return response()->json([
            'data' => $categoriesCollection,
            'message' => 'success'
        ],Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $categories = $this->category->create($request->all());
        $categoriesResource = new CategoryResource($categories);
        return response()->json([
            'data' => $categoriesResource,
            'message' => 'add success'
        ],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $categories = $this->category->findOrFail($id);
        $categoriesResource = new CategoryResource($categories);
        return response()->json([
            'data' => $categoriesResource,
            'message' => 'success'
        ],Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $categories = $this->category->findOrFail($id);
        $categories->update($request->all());
        $categoriesResource = new CategoryResource($categories);
        return response()->json([
            'data' => $categoriesResource,
            'message' => 'update success'
        ],Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $categories = $this->category->findOrFail($id);
        $categories->delete();
        $categoriesResource = new CategoryResource($categories);
        return response()->json([
            'data' => $categoriesResource,
            'message' => 'delete success'
        ],Response::HTTP_OK);
    }
}
