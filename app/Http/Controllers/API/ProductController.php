<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected Product $product;

    /**
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = $this->product->paginate(10);
        $productsCollection = ProductResource::collection($products)->response()->getData(true);
        return response()->json([
            'data' => $productsCollection,
            'message' => 'success'
        ],Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $products = $this->product->create($request->all());
        $productsResource = new ProductResource($products);
        return response()->json([
            'data' => $productsResource,
            'message' => 'add success'
        ],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $products = $this->product->findOrFail($id);
        $productsResource = new ProductResource($products);
        return response()->json([
            'data' => $productsResource,
            'message' => 'success'
        ],Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $products = $this->product->findOrFail($id);
        $products->update($request->all());
        $productsResource = new ProductResource($products);
        return response()->json([
            'data' => $productsResource,
            'message' => 'update success'
        ],Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $products = $this->product->findOrFail($id);
        $products->delete();
        $productsResource = new ProductResource($products);
        return response()->json([
            'data' => $productsResource,
            'message' => 'delete success'
        ],Response::HTTP_OK);
    }
}
