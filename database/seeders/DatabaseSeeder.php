<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->hasAttached(
            Role::factory()->sequence(
                [
                    'name' => 'Admin',
                ],
            )->create(),
            ['role_id' => 1],
            'roles'
        )->create([
            'name' => 'admin',
            'email' => 'hoangAdmin@gmail.com',
            'password' => bcrypt('admin'),
            'activated' => 1
        ]);

        $this->call([
            PermissionSeeder::class
        ]);

    }
}
