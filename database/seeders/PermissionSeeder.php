<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $routes = Route::getRoutes();
        $arrNameRoutes = [];
        foreach ($routes as $value) {
            $arrNameRoutes[] = $value->getName();
        }
        $arrPermission = array_filter($arrNameRoutes, function ($value){
            if (!empty($value) ){
                if (strpos($value, 'index') || strpos($value, 'show') || strpos($value, 'create') || strpos($value, 'update') || strpos($value, 'destroy'))
                {
                    return $value;
                }
            }
        });

        $arrPermission = array_values($arrPermission);

        foreach ($arrPermission as $permission)
        {
            Permission::factory()->create([
                'name' => $permission,
                'route_name' => $permission
            ]);
        }

    }
}
