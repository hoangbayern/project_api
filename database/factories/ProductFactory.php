<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;

    use WithFaker;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'image' => $this->faker->imageUrl(150, 150),
            'description' => $this->faker->paragraph(),
            'stock' => $this->faker->randomNumber(2),
            'price' => $this->faker->randomNumber(6),
            'list_color' => $this->faker->name,
            'gender' => $this->faker->boolean(),
            'activated' => $this->faker->boolean(),
        ];
    }
}
